package wordCounter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import phoneBook.Contract;

public class WordCounter {
	private String message;
	HashMap<String,Integer> wordCount = new HashMap();
	
	public WordCounter(String filename){
		FileReader fileReader = null; 
		Contract c;
		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			String amessage= "";
			
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				amessage+=line;

			}
			message = amessage;
//			System.out.println(message);

		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (fileReader != null)
					fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
		
	}
	public void count() {
		String[] part = message.split(" ");
		for(String a : part){
			if(wordCount.containsKey(a)){
				wordCount.put(a, wordCount.get(a)+1);
			}
			else{
				wordCount.put(a, 1);
			}
		}
	}
	public int hasWord(String word){
		if(wordCount.containsKey(word)){
			int a = wordCount.get(word);
			return a;
		}
		return 0;
	}
	public String getCountData() {
		String str="";
		Set<Map.Entry<String, Integer>> entries = wordCount.entrySet();
		for(Map.Entry<String, Integer> entry : entries){
			str+= entry.getKey()+"\t"+entry.getValue()+"\n";
		}
		return str;
	}

}

