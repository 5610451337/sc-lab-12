package phoneBook;

public class Contract {
	private String number;
	private String name;
	
	public Contract(String name,String number){
		this.name = name;
		this.number = number;
	}
	
	
	
	public String getName(){
		return name;
	}
	
	public String getNumber(){
		return number;
	}

}
