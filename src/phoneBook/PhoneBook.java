package phoneBook;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class PhoneBook {
	ArrayList<Contract> contracts = new ArrayList() ;
	FileWriter fileWriter = null;
	
	public PhoneBook(){
		String filename = "phonebook.txt";
		FileReader fileReader = null; 
		Contract c;
		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] s = line.split(", ");
				
				c = new Contract(s[0],s[1]); 
				contracts.add(c);

			}
			System.out.println(this.toString());

		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (fileReader != null)
					fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
	}
	
	
	public String toString(){
		String str = "--------- Java Phone Book ---------"+"\n";
		str +="Name"+"\tPhone"+"\n";
		for(Contract c :contracts){
			str += c.getName()+"\t"+c.getNumber()+"\n";
		}
		return str;	
	}
	
	

}
