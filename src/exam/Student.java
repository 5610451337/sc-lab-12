package exam;

public class Student {
	private String name;
	private double[] scores = new double[5];
	private double averageScores ;
	private double[] exam = new double[2];
	private double averageExam ;
	
	
	public Student(String name ,double[] scores){
		this.name = name;
		this.scores= scores;

	}
	public void setExam(double[] exam){
		this.exam = exam;
	}
	public void Average(){
		double x =0;
		for(int i = 0;i<scores.length;i++){
			x+=scores[i];
		}
		averageScores = x/5;
	}
	public void AverageExam(){
		double x =0;
		for(int i = 0;i<exam.length;i++){
			x+=exam[i];
		}
		averageExam = x/2;
	}
	
	
	
	public String getName(){
		return name;
	}
	
	public double getAverageScores(){
		return averageScores;
	}
	
	public double getAverageExam(){
		return averageExam;
	}
}

