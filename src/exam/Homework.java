package exam;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Homework {
	private String name;
	private double[] scores = new double[5];
	private double averageScores ;
	ArrayList<Student> students= new ArrayList();
	
	public Homework(){
		String filename = "homework.txt";
		String filename2 = "average.txt";
		String filename3  = "exam.txt";
		FileReader fileReader = null;
		FileReader fileReader2 = null;
		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			
			fileReader2 = new FileReader(filename3);
			BufferedReader buffer2 = new BufferedReader(fileReader2);
			
			String line;
			double[] sc = new double[5];


			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] s = line.split(", ");
				for(int i= 1 ;i<s.length;i++){
					sc[i-1]=Double.parseDouble(s[i]);
				}
				Student stu = new Student(s[0],sc);
				stu.Average();
				students.add(stu);
			}
			
			double[] ex = new double[2];
			for(line = buffer2.readLine(); line != null; line = buffer2.readLine()){
				String[] s = line.split(", ");
				for(Student st : students){
					if(st.getName().equals(s[0])){
						for(int i= 1 ;i<s.length;i++){
							ex[i-1]=Double.parseDouble(s[i]);
						}
						st.setExam(ex);
						st.AverageExam();
					}
					
				}
			}
			FileWriter fileWriter = new FileWriter(filename2);
			PrintWriter out = new PrintWriter(fileWriter);
			out.println(this.toString());
			out.println(this.toStringExam());
			out.flush();
//			System.out.println(this.toString());

		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (fileReader != null )
					fileReader.close();
				if(fileReader2 != null)
					fileReader2.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
		
	}
	
	public String toString(){
		String str = "--------- Homework Scores ---------"+"\n";
		str += "Name"+"\tAverage"+"\n";
		str += "===="+ "\t======="+"\n";
		for(Student s :students){
			str += s.getName()+"\t"+s.getAverageScores()+"\n";
		}
		return str;	
	}
	
	public String toStringExam(){
		String str = "--------- Exam Scores --------- "+"\n";
		str += "Name"+"\tAverage"+"\n";
		str += "===="+ "\t======="+"\n";
		for(Student s :students){
			str += s.getName()+"\t"+s.getAverageExam()+"\n";
		}
		return str;	
		
	}
	
	
	
}

