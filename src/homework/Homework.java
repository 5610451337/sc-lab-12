package homework;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Homework {
	private String name;
	private double[] scores = new double[5];
	private double averageScores ;
	ArrayList<Student> students= new ArrayList();
	
	public Homework(){
		String filename = "homework.txt";
		String filename2 = "average.txt";
		
		FileReader fileReader = null; 

		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			

			
			String line;
			double[] sc = new double[5];

			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] s = line.split(", ");
				for(int i= 1 ;i<s.length;i++){
					sc[i-1]=Double.parseDouble(s[i]);
				}
				Student stu = new Student(s[0],sc);
				stu.Average();
				students.add(stu);
			}
			FileWriter fileWriter = new FileWriter(filename2);
			PrintWriter out = new PrintWriter(fileWriter);
			out.println(this.toString());
			out.flush();
//			System.out.println(this.toString());

		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (fileReader != null)
					fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
		
	}
	
	public String toString(){
		String str = "--------- Homework Scores ---------"+"\n";
		str += "Name"+"\tAverage"+"\n";
		str += "===="+ "\t======="+"\n";
		for(Student s :students){
			str += s.getName()+"\t"+s.getAverageScores()+"\n";
		}
		return str;	
	}
	
	
}
