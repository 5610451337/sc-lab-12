package homework;

public class Student {
	private String name;
	private double[] scores = new double[5];
	private double averageScores ;
	
	
	public Student(String name ,double[] scores){
		this.name = name;
		this.scores= scores;
	}
	
	public void Average(){
		double x =0;
		for(int i = 0;i<scores.length;i++){
			x+=scores[i];
		}
		averageScores = x/5;
	}
	
	public String getName(){
		return name;
	}
	
	public double getAverageScores(){
		return averageScores;
	}
}
